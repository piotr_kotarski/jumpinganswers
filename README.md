Tematyka projektu:​
Gra pt. "Jumping Answers" w stylu "Alto's Adventure", gdzie zjeżdżając z górki na nartach ludzik musi pokonywać przeszkody. Podczas pokonywania przeszkody wyświetla się okno, w którym są pytania, na które odpowiadając wybieramy wariant akrobacji: zwykły skok lub salto.
Pytania z ortografii z możliwością dodawania pytań matematycznych.
Technologia wykonania:​
Visual Studio, Windows Forms, C#​
Cel projektu:​
Utworzenie gry platformowej dla nieograniczonej grupy wiekowej, gdzie poprzez relaksującą rozgrywkę można sprawdzać i poszerzać swoją wiedzę.​
