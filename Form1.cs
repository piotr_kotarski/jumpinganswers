using System;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TrayNotify;

namespace projekt_jpwp
{
    public partial class JumpingAnswers : Form
    {
        public int animationStep = 0;
        public int key_status = 0;
        public int enterCounter1 = 0;
        public int enterCounter2 = 0;
        public int lineIndex1 = 0;
        public int lineIndex2 = 0;
        public const int wordIndex = 0;
        public string question = null;
        public string answer = null;
        public bool salto_status1 = false;
        public bool salto_status2 = false;
        public int points = 0;
        public int gameover = 0;
        public JumpingAnswers()
        {
            InitializeComponent();
            //inicjalizacja timera glownego
            timer_main.Interval = 200;
            timer_main.Start();
            //inicjalizacja timera do skoku
            timer_jump.Interval = 200;
            //inicjalne polozenie obiektow
            rock.Location = new Point(ClientSize.Width, (ClientSize.Height - rock.Height / 3));
            rock2.Location = new Point(595, 306);
            tree.Location = new Point(ClientSize.Width - ClientSize.Width / 4, (ClientSize.Height - tree.Height / 3));

        }

        private void timer_main_Tick(object sender, EventArgs e)
        {
            MoveRock();
            MoveTree();
        }

        private void timer_jump_Tick(object sender, EventArgs e)
        {
            JumpAnimation();
        }

        private void timer_salto_Tick(object sender, EventArgs e)
        {
            SaltoAnimation();
        }
        private void MoveRock()
        {
            //przesuniecie
            rock.Location = new Point(rock.Location.X - 29, rock.Location.Y - 11);
            rock2.Location = new Point(rock2.Location.X - 29, rock2.Location.Y - 11);
            //warunek dojscia obiektu do granicy okna => reset polozenia
            if (rock.Location.X <= 0 - rock.Width)
            {
                rock.Location = new Point(ClientSize.Width, (ClientSize.Height - rock.Height / 3));
            }
            if (rock2.Location.X <= 0 - rock2.Width)
            {
                rock2.Location = new Point(ClientSize.Width, (ClientSize.Height - rock2.Height / 3));
            }
        }

        private void MoveTree()
        {
            //przesuniecie
            tree.Location = new Point(tree.Location.X - 29, tree.Location.Y - 11);
            //warunek dojscia obiektu do granicy okna => reset polozenia
            if (tree.Location.X <= 0 - tree.Width)
            {
                tree.Location = new Point(ClientSize.Width - ClientSize.Width / 4, (ClientSize.Height - tree.Height / 3));
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            // sprawdzanie nacisniecia klawisza spacji
            if (e.KeyCode == Keys.Space)
            {
                key_status++;
                if (key_status % 2 == 0)
                {
                    Quiz();
                    timer_main.Stop();
                }
                else
                {
                    quiz_panel.Hide();
                    timer_main.Start();
                    timer_jump.Start();
                }
            }
        }

        private void JumpAnimation()
        {
            if (animationStep < 5)
            {
                skier.Location = new Point(skier.Location.X, skier.Location.Y - 20);
                animationStep++;
            }
            else if (animationStep is >= 5 and < 10)
            {
                skier.Location = new Point(skier.Location.X, skier.Location.Y + 20);
                animationStep++;
            }
            else
            {
                timer_jump.Stop();
                animationStep = 0;
            }
        }

        private void SaltoAnimation()
        {
            if (animationStep < 5)
            {
                skier.Location = new Point(skier.Location.X, skier.Location.Y - 20);
                animationStep++;
            }
            else if (animationStep is >= 5 and < 10)
            {
                switch (animationStep)
                {
                    case 5:
                        skier.Image = Properties.Resources.skier90;
                        break;
                    case 6:
                        skier.Image = Properties.Resources.skier180;
                        break;
                    case 7:
                        skier.Image = Properties.Resources.skier270;
                        break;
                    case 8:
                        skier.Image = Properties.Resources.skier;
                        break;
                }
                skier.Location = new Point(skier.Location.X, skier.Location.Y + 20);
                animationStep++;
            }
            else
            {
                timer_salto.Stop();
                animationStep = 0;
            }
        }

        private void Quiz()
        {
            gameover = 0;
            salto_status1 = false;
            salto_status2 = false;
            enterCounter1 = 0;
            enterCounter2 = 0;
            randomIndex();
            quiz_panel.Show();
            ShowQuestion(GetFromFile(lineIndex1, wordIndex), GetFromFile(lineIndex2, wordIndex));
            //CheckAnswers();
        }

        private string GetFromFile(int line_number, int word)
        {
            string filePath = "C:/Users/piotr/source/repos/projekt_jpwp/Resources/questions.txt";
            int LineNumber = line_number;
            int WordIndex = word;
            string quest_ans = null;

            try
            {
                using (StreamReader sr = new StreamReader(filePath))
                {
                    for (int lineNumber = 1; lineNumber <= LineNumber; lineNumber++)
                    {
                        string line = sr.ReadLine();

                        if (lineNumber == LineNumber)
                        {
                            string[] words = line.Split('\t');

                            if (words.Length > WordIndex)
                            {
                                string desiredWord = words[WordIndex];

                                quest_ans = desiredWord;
                            }
                            else
                            {
                                quest_ans = "Niepoprawny indeks s�owa w linii.";
                            }
                        }
                    }
                }
            }
            catch (IOException e)
            {
                quest_ans = "Wyst�pi� b��d odczytu pliku: " + e.Message;
            }
            return (quest_ans);
        }

        private void ShowQuestion(string question_1, string question_2)
        {
            question1.Text = question_1;
            question2.Text = question_2;
        }

        public void randomIndex()
        {
            string filePath = "C:/Users/piotr/source/repos/projekt_jpwp/Resources/questions.txt";

            try
            {
                Random random = new Random();

                string[] lines = File.ReadAllLines(filePath);

                int numberOfLines = lines.Length;
                do
                {
                    lineIndex1 = random.Next(1, numberOfLines + 1);
                    lineIndex2 = random.Next(1, numberOfLines + 1);
                }
                while (lineIndex1 == lineIndex2);
            }
            catch (IOException e) { }
        }

        public bool checkAnswer(string ans, string correct_ans)
        {
            if (ans == correct_ans)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void displayPoints(int pts)
        {
            points = pts + points;
            points_label.Text = "PUNKTY: " + points.ToString();
        }

        public void GameOver()
        {
            gameover++;
            if (gameover % 2 == 0)
            {
                timer_jump.Stop();
                timer_main.Stop();
                timer_salto.Stop();
                quiz_panel.Hide();
                skier.Hide();
                tree.Hide();
                rock.Hide();
                rock2.Hide();
                gameover_label.Show();
            }
        }
        private void question1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (enterCounter1 % 2 == 0)
                {
                    bool ans = checkAnswer(question1.Text, GetFromFile(lineIndex1, wordIndex + 1));
                    if (ans == true)
                    {
                        question1.BackColor = Color.Green;
                        salto_status1 = true;
                        displayPoints(10);
                    }
                    else
                    {
                        question1.BackColor = Color.Red;
                        salto_status1 = false;
                        GameOver();
                    }
                }
                else
                {
                    key_status++;
                    quiz_panel.Hide();
                    timer_main.Start();
                    switch (salto_status1==salto_status2)
                    {
                        case true:
                            timer_salto.Start();
                            break;
                        case false:
                            timer_jump.Start();
                            break;
                    }
                    question1.BackColor = Color.Azure;
                    question2.BackColor = Color.Azure;
                }
            }
            enterCounter1++;
            this.Focus();
        }

        private void question2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (enterCounter2 % 2 == 0)
                {
                    bool ans = checkAnswer(question2.Text, GetFromFile(lineIndex2, wordIndex + 1));
                    if (ans == true)
                    {
                        question2.BackColor = Color.Green;
                        salto_status2 = true;
                        displayPoints(10);
                    }
                    else
                    {
                        question2.BackColor = Color.Red;
                        salto_status2 = false;
                        GameOver();
                    }
                }
                else
                {
                    key_status++;
                    quiz_panel.Hide();
                    timer_main.Start();
                    switch (salto_status1 == salto_status2)
                    {
                        case true:
                            timer_salto.Start();
                            break;
                        case false:
                            timer_jump.Start();
                            break;
                    }
                    question2.BackColor = Color.Azure;
                    question1.BackColor = Color.Azure;
                }
            }
            enterCounter2++;
            this.Focus();
        }
    }
}
