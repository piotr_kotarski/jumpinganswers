﻿namespace projekt_jpwp
{
    partial class JumpingAnswers
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JumpingAnswers));
            rock = new PictureBox();
            skier = new PictureBox();
            timer_main = new System.Windows.Forms.Timer(components);
            tree = new PictureBox();
            timer_jump = new System.Windows.Forms.Timer(components);
            rock2 = new PictureBox();
            quiz_panel = new Panel();
            answer_instructions = new Label();
            question2 = new TextBox();
            question1 = new TextBox();
            question2_label = new Label();
            question1_label = new Label();
            title = new Label();
            timer_salto = new System.Windows.Forms.Timer(components);
            points_label = new Label();
            gameover_label = new Label();
            ((System.ComponentModel.ISupportInitialize)rock).BeginInit();
            ((System.ComponentModel.ISupportInitialize)skier).BeginInit();
            ((System.ComponentModel.ISupportInitialize)tree).BeginInit();
            ((System.ComponentModel.ISupportInitialize)rock2).BeginInit();
            quiz_panel.SuspendLayout();
            SuspendLayout();
            // 
            // rock
            // 
            rock.BackColor = Color.Transparent;
            rock.BackgroundImageLayout = ImageLayout.Center;
            rock.Image = Properties.Resources.rock;
            rock.Location = new Point(640, 322);
            rock.Name = "rock";
            rock.Size = new Size(106, 74);
            rock.SizeMode = PictureBoxSizeMode.Zoom;
            rock.TabIndex = 0;
            rock.TabStop = false;
            // 
            // skier
            // 
            skier.BackColor = Color.Transparent;
            skier.BackgroundImageLayout = ImageLayout.None;
            skier.Image = Properties.Resources.skier;
            skier.Location = new Point(606, 304);
            skier.Name = "skier";
            skier.Size = new Size(95, 102);
            skier.SizeMode = PictureBoxSizeMode.StretchImage;
            skier.TabIndex = 1;
            skier.TabStop = false;
            // 
            // timer_main
            // 
            timer_main.Tick += timer_main_Tick;
            // 
            // tree
            // 
            tree.BackColor = Color.Transparent;
            tree.Image = Properties.Resources.tree;
            tree.Location = new Point(370, 294);
            tree.Name = "tree";
            tree.Size = new Size(143, 250);
            tree.SizeMode = PictureBoxSizeMode.Zoom;
            tree.TabIndex = 2;
            tree.TabStop = false;
            // 
            // timer_jump
            // 
            timer_jump.Tick += timer_jump_Tick;
            // 
            // rock2
            // 
            rock2.BackColor = Color.Transparent;
            rock2.BackgroundImageLayout = ImageLayout.Center;
            rock2.Image = Properties.Resources.rock;
            rock2.Location = new Point(595, 306);
            rock2.Name = "rock2";
            rock2.Size = new Size(106, 74);
            rock2.SizeMode = PictureBoxSizeMode.Zoom;
            rock2.TabIndex = 3;
            rock2.TabStop = false;
            // 
            // quiz_panel
            // 
            quiz_panel.BackColor = Color.Azure;
            quiz_panel.Controls.Add(answer_instructions);
            quiz_panel.Controls.Add(question2);
            quiz_panel.Controls.Add(question1);
            quiz_panel.Controls.Add(question2_label);
            quiz_panel.Controls.Add(question1_label);
            quiz_panel.Controls.Add(title);
            quiz_panel.Location = new Point(252, 49);
            quiz_panel.Name = "quiz_panel";
            quiz_panel.Size = new Size(758, 494);
            quiz_panel.TabIndex = 4;
            quiz_panel.Visible = false;
            // 
            // answer_instructions
            // 
            answer_instructions.AutoSize = true;
            answer_instructions.Font = new Font("Segoe UI Semibold", 9F, FontStyle.Bold, GraphicsUnit.Point, 238);
            answer_instructions.Location = new Point(190, 350);
            answer_instructions.Name = "answer_instructions";
            answer_instructions.Size = new Size(374, 80);
            answer_instructions.TabIndex = 5;
            answer_instructions.Text = "*Aby udzielić odpowiedź na pytanie należy:\r\n - podmienić znak _ bądź X na prawidłową odpowiedź\r\n - nacisnąć klawisz enter\r\n*Aby pominąć pytanie należy kliknąć klawisz spacji.";
            answer_instructions.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // question2
            // 
            question2.BackColor = Color.Azure;
            question2.BorderStyle = BorderStyle.FixedSingle;
            question2.Location = new Point(261, 292);
            question2.Name = "question2";
            question2.Size = new Size(237, 27);
            question2.TabIndex = 4;
            question2.TextAlign = HorizontalAlignment.Center;
            question2.KeyDown += question2_KeyDown;
            // 
            // question1
            // 
            question1.BackColor = Color.Azure;
            question1.BorderStyle = BorderStyle.FixedSingle;
            question1.Location = new Point(261, 189);
            question1.Name = "question1";
            question1.Size = new Size(237, 27);
            question1.TabIndex = 3;
            question1.TextAlign = HorizontalAlignment.Center;
            question1.KeyDown += question1_KeyDown;
            // 
            // question2_label
            // 
            question2_label.AutoSize = true;
            question2_label.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point, 238);
            question2_label.Location = new Point(326, 242);
            question2_label.Name = "question2_label";
            question2_label.Size = new Size(106, 28);
            question2_label.TabIndex = 2;
            question2_label.Text = "Pytanie 2:";
            // 
            // question1_label
            // 
            question1_label.AutoSize = true;
            question1_label.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point, 238);
            question1_label.Location = new Point(326, 134);
            question1_label.Name = "question1_label";
            question1_label.Size = new Size(106, 28);
            question1_label.TabIndex = 1;
            question1_label.Text = "Pytanie 1:";
            // 
            // title
            // 
            title.AutoSize = true;
            title.Font = new Font("Segoe UI", 24F, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, 238);
            title.Location = new Point(320, 41);
            title.Name = "title";
            title.Size = new Size(119, 54);
            title.TabIndex = 0;
            title.Text = "QUIZ";
            // 
            // timer_salto
            // 
            timer_salto.Interval = 200;
            timer_salto.Tick += timer_salto_Tick;
            // 
            // points_label
            // 
            points_label.AutoSize = true;
            points_label.BackColor = Color.Transparent;
            points_label.Font = new Font("Segoe UI", 12F, FontStyle.Bold, GraphicsUnit.Point, 238);
            points_label.Location = new Point(1136, 9);
            points_label.Name = "points_label";
            points_label.Size = new Size(114, 28);
            points_label.TabIndex = 5;
            points_label.Text = "PUNKTY: 0";
            // 
            // gameover_label
            // 
            gameover_label.AutoSize = true;
            gameover_label.BackColor = Color.Transparent;
            gameover_label.Font = new Font("Segoe UI", 72F, FontStyle.Bold, GraphicsUnit.Point, 238);
            gameover_label.ForeColor = Color.Red;
            gameover_label.Location = new Point(251, 217);
            gameover_label.Name = "gameover_label";
            gameover_label.Size = new Size(761, 159);
            gameover_label.TabIndex = 6;
            gameover_label.Text = "GAME OVER";
            gameover_label.TextAlign = ContentAlignment.MiddleCenter;
            gameover_label.Visible = false;
            // 
            // JumpingAnswers
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            BackgroundImage = (Image)resources.GetObject("$this.BackgroundImage");
            BackgroundImageLayout = ImageLayout.Stretch;
            ClientSize = new Size(1262, 593);
            Controls.Add(gameover_label);
            Controls.Add(points_label);
            Controls.Add(quiz_panel);
            Controls.Add(skier);
            Controls.Add(rock);
            Controls.Add(tree);
            Controls.Add(rock2);
            MaximumSize = new Size(1280, 640);
            MinimumSize = new Size(1280, 640);
            Name = "JumpingAnswers";
            Text = "JumpingAnswers";
            ((System.ComponentModel.ISupportInitialize)rock).EndInit();
            ((System.ComponentModel.ISupportInitialize)skier).EndInit();
            ((System.ComponentModel.ISupportInitialize)tree).EndInit();
            ((System.ComponentModel.ISupportInitialize)rock2).EndInit();
            quiz_panel.ResumeLayout(false);
            quiz_panel.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private PictureBox tree;
        private PictureBox rock;
        private PictureBox skier;
        private System.Windows.Forms.Timer timer_main;
        private System.Windows.Forms.Timer timer_jump;
        private PictureBox rock2;
        private Panel quiz_panel;
        public Label title;
        private Label question2_label;
        private Label question1_label;
        private TextBox question1;
        private TextBox question2;
        private Label answer_instructions;
        private System.Windows.Forms.Timer timer_salto;
        private Label points_label;
        private Label gameover_label;
    }
}
